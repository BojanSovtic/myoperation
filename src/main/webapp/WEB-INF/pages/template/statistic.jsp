<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page isELIgnored="false"%>

<div>
  <table>
    <tr>
      <th>URL</th>
      <th>Visited</th>
    </tr>
    <c:forEach var="item" items="${stats}">
      <tr>
        <td>${item.key}</td>
        <td>${item.value}</td>
      </tr>
    </c:forEach>
  </table>
</div>