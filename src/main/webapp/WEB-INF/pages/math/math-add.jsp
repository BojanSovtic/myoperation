<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
  
  <jsp:include page="/WEB-INF/pages/template/navigation.jsp"></jsp:include>
  <jsp:include page="/WEB-INF/pages/template/statistic.jsp"></jsp:include>
    
  <p>${message}</p>
  
  <form action="/myoperation/app/math" method="POST">
    <table>
      <tr>
        <td><label for="a">a: </label></td>
        <td><input type="text" id="a" name="a" value="${math.valueA}" ></td>
      </tr>
      <tr>
        <td colspan="2">${errorA}</td>
      </tr>
      
      
      <tr>
        <td><label for="b">b:</label></td>
        <td><input type="text" id="b" name="b" value="${math.valueB}" ></td>
      </tr>
       <tr>
        <td colspan="2">${errorB}</td>
      </tr>
      
      <tr>
        <td><label for="c">c:</label></td>
        <td><input type="text" id="c" name="c" value="${math.result}" readonly></td>
      </tr>
      <tr>
        <td><label for="operation">operacija:</label></td>
        <td><select id="operation" name="operation">
            <!-- dinamicki popuniti -->
            <c:forEach var="mathOper" items="${request_operations}">
              <option value="${mathOper.sign}">${mathOper.name}</option>
            </c:forEach>
        </select></td>
      </tr>

      <!-- dugme save i dugme calculate -->
      <tr>
        <td><input type="submit" id="actionSave" name="action" value="save"></td>
        <td><input type="submit" id="actionCalculate" name="action" value="calculate"></td>
      </tr>
    </table>
  </form>


</body>
</html>