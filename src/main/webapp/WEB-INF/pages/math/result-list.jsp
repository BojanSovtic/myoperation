<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page isELIgnored="false"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Result list</title>

<style>
  #operation {
    text-align: center;
  }
  
  #result {
    text-align: right;
  }

</style>

</head>
<body>

  <jsp:include page="/WEB-INF/pages/template/navigation.jsp"></jsp:include>
  <jsp:include page="/WEB-INF/pages/template/statistic.jsp"></jsp:include>

  <h1>Result list</h1>

  <c:if test="${context_results.size() > 0}">

    <table>
      <thead>
        <tr>
          <th>A</th>
          <th>Operation</th>
          <th>B</th>
          <th>Result</th>
        </tr>
      </thead>
      <tbody>
        <c:forEach var="result" items="${context_results}">
          <tr>
            <td>${result.valueA}</td>
            <td id="operation">${result.operation}</td>
            <td>${result.valueB}</td>
            <td id="result">${result.result}</td>
          </tr>
        </c:forEach>
      </tbody>
    </table>

  </c:if>

</body>
</html>