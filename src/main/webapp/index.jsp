<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Home page</title>
</head>
<body>
  
  <jsp:include page="/WEB-INF/pages/template/navigation.jsp"></jsp:include>
  <jsp:include page="/WEB-INF/pages/template/statistic.jsp"></jsp:include>

  <h2>Home page</h2>
  
</body>
</html>
