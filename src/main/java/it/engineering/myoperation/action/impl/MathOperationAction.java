package it.engineering.myoperation.action.impl;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import it.engineering.myoperation.action.AbstractAction;
import it.engineering.myoperation.dto.MathOperationDto;
import it.engineering.myoperation.model.MathOperation;
import it.engineering.myoperation.util.Params;

public class MathOperationAction extends AbstractAction {

	@Override
	public String execute(HttpServletRequest request) {	
		String action = request.getParameter("action");
		String strA = request.getParameter("a");
		String strB = request.getParameter("b");
		String operation = request.getParameter("operation");
		
		double a = 0;
		boolean validation = true;
		try {
			a = Double.parseDouble(strA);
		} catch (NumberFormatException nfe) {
			validation = false;
			request.setAttribute("errorA", "U polje a unesite broj!");
		}

		double b = 0;
		try {
			b = Double.parseDouble(strB);
		} catch (NumberFormatException nfe) {
			validation = false;
			request.setAttribute("errorB", "U polje b unesite broj!");
		}

		double result = 0;
		switch (operation) {
		case "+":
			result = a + b;
			break;

		case "-":
			result = a - b;
			break;

		case "/":
			if (b == 0) {
				validation = false;
				request.setAttribute("errorB", "U polje b je uneta 0 za operaciju deljenja!");
			} else {
				result = a / b;
			}
			break;

		case "*":
			result = a * b;
			break;

		default:
			break;
		}
		
		if (action.equalsIgnoreCase("save")) {
			if (validation) {
				((ArrayList<MathOperationDto>) request.getServletContext().getAttribute("context_results"))
					.add(new MathOperationDto(strA, strB, String.format("%.2f", result), operation));
				
				return Params.VIEW_RESULTS_SHOW;
			} else {
				MathOperationDto mathOperationDto = new MathOperationDto(strA, strB, "N/A", operation);
				request.setAttribute("math", mathOperationDto);
				request.setAttribute("message", "Operacija nije sacuvana, unesite validnu operaciju!");
				request.setAttribute("request_operations", createOperation());
				
				return Params.VIEW_MATH_SHOW_OPERATIONS;
			}
		} else if (action.equalsIgnoreCase("calculate")) {
			MathOperationDto mathOperationDto = new MathOperationDto(strA, strB,
					(validation) ? String.valueOf(result) : "N/A", operation);
			request.setAttribute("math", mathOperationDto);
			request.setAttribute("message", (validation) ? "Operacija je uspesno izvrsena"
					: "Operacija nije izvrsena, postoji greska u unosu!");
			request.setAttribute("request_operations", createOperation());
			
			return Params.VIEW_MATH_SHOW_OPERATIONS;
		} else {
			return Params.VIEW_DEFAULT_ERROR;
		}
	}

	private List<MathOperation> createOperation() {
		return new ArrayList<MathOperation>() {
			{
				add(new MathOperation("+", "sabiranje"));
				add(new MathOperation("-", "oduzimanje"));
				add(new MathOperation("/", "deljenje"));
				add(new MathOperation("*", "mnozenje"));
			}
		};
	}

}
