package it.engineering.myoperation.action.impl;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import it.engineering.myoperation.action.AbstractAction;
import it.engineering.myoperation.dto.MathOperationDto;
import it.engineering.myoperation.model.MathOperation;
import it.engineering.myoperation.util.Params;

public class MathOperationShowAction extends AbstractAction {

	@Override
	public String execute(HttpServletRequest request) {
		request.setAttribute("message", "Izvrsite zeljenu operaciju");
		request.setAttribute("request_operations", createOperation());
		request.setAttribute("math", new MathOperationDto("0", "0", "0", ""));
		
		return Params.VIEW_MATH_SHOW_OPERATIONS;
	}
	
	private List<MathOperation> createOperation() {
		return new ArrayList<MathOperation>() {
			{
				add(new MathOperation("+", "sabiranje"));
				add(new MathOperation("-", "oduzimanje"));
				add(new MathOperation("/", "deljenje"));
				add(new MathOperation("*", "mnozenje"));
			}
		};
	}
	
}
