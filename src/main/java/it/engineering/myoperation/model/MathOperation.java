package it.engineering.myoperation.model;

import java.io.Serializable;

public class MathOperation implements Serializable {

	private static final long serialVersionUID = 1L;

	private String sign;
	private String name;

	public MathOperation() {
	}

	public MathOperation(String sign, String name) {
		this.sign = sign;
		this.name = name;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
