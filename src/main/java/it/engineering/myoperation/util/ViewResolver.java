package it.engineering.myoperation.util;

import java.util.HashMap;
import java.util.Map;

public class ViewResolver {
	
	private Map<String, String> maps;
	
	public ViewResolver() {
		maps = new HashMap<String, String>();
		maps.put(Params.VIEW_MATH_SHOW_OPERATIONS, Params.PAGE_MATH_SHOW_OPERATIONS);
		maps.put(Params.VIEW_DEFAULT_ERROR, Params.PAGE_DEFAULT_ERROR);
		maps.put(Params.VIEW_RESULTS_SHOW, Params.PAGE_RESULTS_SHOW);
	}
	
	public String resolve(String key) {
		return maps.get(key);
	}
}
