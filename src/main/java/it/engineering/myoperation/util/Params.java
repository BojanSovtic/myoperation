package it.engineering.myoperation.util;

public class Params {
	
	public static final String VIEW_DEFAULT_ERROR = "view-default-error";
	public static final String PAGE_DEFAULT_ERROR = "/WEB-INF/pages/error/default-error-page.jsp";
	
	public static final String PATH_MATH_SHOW_OPERATIONS = "/math-operations";
	public static final String VIEW_MATH_SHOW_OPERATIONS = "view-math-operations";
	public static final String PAGE_MATH_SHOW_OPERATIONS = "/WEB-INF/pages/math/math-add.jsp";
	
	public static final String PATH_MATH_OPERATIONS = "/math";
	
	public static final String VIEW_RESULTS_SHOW = "view-results-show";
	public static final String PAGE_RESULTS_SHOW = "/WEB-INF/pages/math/result-list.jsp";
}
