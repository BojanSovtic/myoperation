package it.engineering.myoperation.listener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import it.engineering.myoperation.dto.MathOperationDto;
import it.engineering.myoperation.model.MathOperation;

/**
 * Application Lifecycle Listener implementation class SetupListener
 *
 */
@WebListener("setup")
public class SetupListener implements ServletContextListener {

    /**
     * Default constructor. 
     */
    public SetupListener() {
    }

	/**
     * @see ServletContextListener#contextDestroyed(ServletContextEvent)
     */
    public void contextDestroyed(ServletContextEvent sce)  { 
    }

	/**
     * @see ServletContextListener#contextInitialized(ServletContextEvent)
     */
    public void contextInitialized(ServletContextEvent sce)  { 
         System.out.println("**************************************************************");
         System.out.println("********************* Kontekst inicijalizovan ****************");
         System.out.println("**************************************************************");
         
         sce.getServletContext().setAttribute("context_operations", createOperation());
         
         sce.getServletContext().setAttribute("context_results", new ArrayList<MathOperationDto>());
         sce.getServletContext().setAttribute("stats", new HashMap<String, Number>());
    }
    
    private List<MathOperation> createOperation() {
    	return new ArrayList<MathOperation>() {
    		{
    			add(new MathOperation("+", "sabiranje"));
    			add(new MathOperation("-", "oduzimanje"));
    			add(new MathOperation("/", "deljenje"));
    			add(new MathOperation("*", "mnozenje"));
    		}
    	};
    }
	
}
