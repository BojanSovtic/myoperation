package it.engineering.myoperation.dto;

import java.io.Serializable;

public class MathOperationDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private String valueA;
	private String valueB;
	private String result;
	private String operation;

	public MathOperationDto() {
	}

	public MathOperationDto(String valueA, String valueB, String result, String operation) {
		this.valueA = valueA;
		this.valueB = valueB;
		this.result = result;
		this.operation = operation;
	}

	public String getValueA() {
		return valueA;
	}

	public void setValueA(String valueA) {
		this.valueA = valueA;
	}

	public String getValueB() {
		return valueB;
	}

	public void setValueB(String valueB) {
		this.valueB = valueB;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

}
