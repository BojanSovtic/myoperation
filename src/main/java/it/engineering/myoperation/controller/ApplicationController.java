package it.engineering.myoperation.controller;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import it.engineering.myoperation.action.AbstractAction;
import it.engineering.myoperation.action.impl.MathOperationAction;
import it.engineering.myoperation.action.impl.MathOperationShowAction;
import it.engineering.myoperation.util.Params;

public class ApplicationController {
	
	public String processRequest(HttpServletRequest request) {
		String view = Params.VIEW_DEFAULT_ERROR;
		String path = request.getPathInfo();
		
		System.out.println(">>>PathInfo: " + path);  // DEBUG
		
		AbstractAction action;
		HashMap<String, Number> statistic = (HashMap<String, Number>) request.getServletContext().getAttribute("stats");
		
		
		if (path.equalsIgnoreCase(Params.PATH_MATH_SHOW_OPERATIONS)) {
			action = new MathOperationShowAction();
			
			incrementURL(request.getContextPath().concat(path), statistic);
			
			return action.execute(request);
		} else if (path.equalsIgnoreCase(Params.PATH_MATH_OPERATIONS)) {
			action = new MathOperationAction();
			
			incrementURL(request.getContextPath().concat(path), statistic);
			
			return action.execute(request);
		}
		
		return view;
	}
	
	private static void incrementURL(String url, HashMap<String, Number> statistic) {
		if (!statistic.containsKey(url)) {
			statistic.put(url, 1);
		}
		
		int count = statistic.get(url).intValue();
		statistic.put(url, ++count);
	}
}
