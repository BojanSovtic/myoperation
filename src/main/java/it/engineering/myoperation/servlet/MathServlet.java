package it.engineering.myoperation.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import it.engineering.myoperation.dto.MathOperationDto;
import it.engineering.myoperation.model.MathOperation;

/**
 * Servlet implementation class MathServlet
 */
@WebServlet(urlPatterns = { "/math", "/mathoper" })
public class MathServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public MathServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setAttribute("message", "Izvrsite zeljenu operaciju");
		request.setAttribute("request_operations", createOperation());
		request.setAttribute("math", new MathOperationDto("0", "0", "0", ""));
		request.getRequestDispatcher("/pages/math-add.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String strA = request.getParameter("a");
		String strB = request.getParameter("b");
		String operation = request.getParameter("operation");
		String action = request.getParameter("action");

		double a = 0;
		boolean validation = true;
		try {
			a = Double.parseDouble(strA);
		} catch (NumberFormatException nfe) {
			validation = false;
			request.setAttribute("errorA", "U polje a unesite broj!");
		}

		double b = 0;
		try {
			b = Double.parseDouble(strB);
		} catch (NumberFormatException nfe) {
			validation = false;
			request.setAttribute("errorB", "U polje b unesite broj!");
		}

		double result = 0;
		switch (operation) {
		case "+":
			result = a + b;
			break;

		case "-":
			result = a - b;
			break;

		case "/":
			if (b == 0) {
				validation = false;
				request.setAttribute("errorB", "U polje b je uneta 0 za operaciju deljenja!");
			} else {
				result = a / b;
			}
			break;

		case "*":
			result = a * b;
			break;

		default:
			break;
		}

		if (action.equals("save")) {
			if (validation) {
				((ArrayList<MathOperationDto>) request.getServletContext().getAttribute("context_results"))
					.add(new MathOperationDto(strA, strB, String.format("%.2f", result), operation));
				request.getRequestDispatcher("/pages/result-list.jsp").forward(request, response);
			} else {
				MathOperationDto mathOperationDto = new MathOperationDto(strA, strB, "N/A", operation);
				request.setAttribute("math", mathOperationDto);
				request.setAttribute("message", "Operacija nije sacuvana, unesite validnu operaciju!");
				request.setAttribute("request_operations", createOperation());
				request.getRequestDispatcher("/pages/math-add.jsp").forward(request, response);
			}
		} else if (action.equals("calculate")) {
			MathOperationDto mathOperationDto = new MathOperationDto(strA, strB,
					(validation) ? String.valueOf(result) : "N/A", operation);
			request.setAttribute("math", mathOperationDto);
			request.setAttribute("message", (validation) ? "Operacija je uspesno izvrsena"
					: "Operacija nije izvrsena, postoji greska u unosu!");
			request.setAttribute("request_operations", createOperation());
			request.getRequestDispatcher("/pages/math-add.jsp").forward(request, response);
		}

	}

	private List<MathOperation> createOperation() {
		return new ArrayList<MathOperation>() {
			{
				add(new MathOperation("+", "sabiranje"));
				add(new MathOperation("-", "oduzimanje"));
				add(new MathOperation("/", "deljenje"));
				add(new MathOperation("*", "mnozenje"));
			}
		};
	}

}
